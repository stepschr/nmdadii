$(document).on("mobileinit", function() {
    console.info("jQuery", $.prototype.jquery
        , "|", "jQuery Mobile", $.mobile.version
        , "|", "Lo-Dash", _.VERSION
//        , "|", "Modernizr", Modernizr._version
    );
    /**
     * jQuery Mobile defaults: http://api.jquerymobile.com/global-config/
     */
    $.mobile.ajaxEnabled = false;
    $.mobile.allowCrossDomainPages = true;/* $.support.cors = true; // needed for Apache Cordova or Adobe PhoneGap */
    $.mobile.loader.prototype.options.html = "";
    $.mobile.loader.prototype.options.textVisible = false;
    $.mobile.loader.prototype.options.theme = "b";
    $.mobile.ns = ""; // Set a namespace for jQuery Mobile data attributes
    $.mobile.page.prototype.options.theme = "a";
    $.mobile.popup.prototype.options.overlayTheme = "b";
    $.ajaxSetup({
        cache: false,
        contentType: "application/json",
        dataType: "json",
        timeout: 30 * 1000 // 30 seconds to timeout
    });


    App.init();



    function LoadOnce()
    {
        window.location.reload();


    }

});

/**
 * App object with properties and methods
 */

var baseUrl = "/nmdad-ii.arteveldehogeschool.be/public/api/";
var App = {
    init: function() {
        this.bindEvents();

        this.loadUsers();
        this.loadDeletedUsers();
        this.loadAllTasks();
        this.loadTasks();
        this.loadFinished();
        this.loadLists();


        console.info("App initialized");

    },

    loadUsers: function(){
        var that = this;
        $.ajax({
            type: "GET",
            url: baseUrl + "users",
            cache: false,
            success: function(userlijst){
                that.renderUsers(userlijst);
                that.renderSearch(userlijst);
                // console.log(userlijst);

            },
            error: function(){

            }
        });
    },

    renderUsers: function(userlijst){
        var that = this;
        //console.log(userlijst);
        var userList = "";
        $.each(userlijst, function(i, user){
            userList += _.template(that.templates.user, {"user": user});
        });
        $("#userlist").html(userList);

    },

    loadDeletedUsers: function(){
        var that = this;
        $.ajax({
            type: "GET",
            url: baseUrl + "deletedusers",
            cache: false,
            success: function(userlijst){
                that.renderDeletedUsers(userlijst);

            },
            error: function(){

            }
        });
    },

    renderDeletedUsers: function(userlijst){
        var that = this;
        var userList = "";
        $.each(userlijst, function(i, user){
            userList += _.template(that.templates.restoreuser, {"user": user});
        });
        $("#deleteduserlist").html(userList);
    },


    loadTasks: function(){
        var that = this;
        $.ajax({
            type: "GET",
            url: baseUrl + "tasklijst",
            cache: false,
            success: function(taskslijst){
                that.renderTasks(taskslijst);
                // console.log(taskslijst);
            },
            error: function(){

            }
        });
    },

    renderTasks: function(taskslijst){
        var that = this;
        var taskList = "";
        if(taskslijst.length>0){

            $.each(taskslijst, function(i, task){
                taskList += _.template(that.templates.task, {"task": task});

            });
        }
        else{
            taskList="Er zijn momenteel geen taken."
        }
        $("#tasks").html(taskList);

    },


    renderSearch:function(userslijst){
        var that = this;


        var usersList = "";
        $.each(userslijst, function(i, user){
            usersList += _.template(that.templates.ddlUser, {"user" : user});
        });


    },

    loadFinished: function(){
        var that = this;
        $.ajax({
            type: "GET",
            url: baseUrl + "finishedlijst",
            cache: false,
            success: function(taskslijst){
                that.renderFinished(taskslijst);

            },
            error: function(){

            }
        });
    },
    loadAllTasks: function(){
        var that = this;
        $.ajax({
            type: "GET",
            url: baseUrl + "alltask",
            cache: false,
            success: function(taskslijst){
                that.renderAllTasks(taskslijst);


            },
            error: function(){

            }
        });
    },
    renderAllTasks: function(taskslijst){
        var that = this;
        var taskList = "";
        $.each(taskslijst, function(i, task){
            taskList += _.template(that.templates.taskAdmin, {"task": task});
        });
        $("#alltask").html(taskList);
    },

    renderFinished: function(taskslijst){
        var that = this;
        var taskList = "";
        $.each(taskslijst, function(i, task){
            taskList += _.template(that.templates.finished, {"task": task});
        });
        if(taskList!=""){
            $("#finishedtasks").html("<h1>Afgewerkte taken</h1>"+taskList);
        }
    },

    loadLists: function(){

        var that = this;
        $.ajax({
            type: "GET",
            url: baseUrl + "list",
            cache: false,
            success: function(lists){
                that.renderLists(lists);



            },
            error: function(){

            }
        });
    },

    renderLists: function(lists){
        var that = this;
        var lijstList = "";
        if(lists.length>0){

            $.each(lists, function(i, list){

                var taskList = "";
                console.log(list);
                if(list.tasks.length>0){
                    $.each(list.tasks, function(j, task) {
                        taskList += _.template(that.templates.tasklist, {"task": task});


                    });
                    lijstList +=
                        $(_.template(that.templates.list, { "list": list }))
                            .find("ul.lijstTodos")
                            .html(taskList)
                            .end()
                            .prop("outerHTML")

                    ;
                }
                else{
                    taskList= "Er zijn geen taken in deze lijst.";
                    lijstList +=
                        $(_.template(that.templates.listDel, { "list": list }))
                            .find("ul.lijstTodos")
                            .html(taskList)
                            .end()
                            .prop("outerHTML")

                    ;
                }




            });

        }
        else{
            lijstList="Er zijn momenteel geen lijsten."
        }
        $("#lijsten").html(lijstList);
    },

    bindEvents: function() {
        var that = this;



        //SCROLL TO TOP
        $('.scrolltotop').click(function(ev){
            ev.preventDefault();
            $('html, body').animate({scrollTop:0},600);
            return false;
        });

        //FADE IN AND OUT SCROLLTOTOP
        $(window).scroll(function(ev){
            if($(this).scrollTop() > 60)
                $('.scrolltotop').fadeIn();
            else
                $('.scrolltotop').fadeOut();
        });

        //SCROLL TO TOP
        $(window).resize(function(ev){
            ev.preventDefault();
            if($(this).width() > 480)
                $('#vb').show(),
                    $('#beschikbaar').show();
            else
                $('#vb').hide(),
                    $('#beschikbaar').hide();
            return false;
        });

        $(window).resize(function(ev){
            ev.preventDefault();
            if($(this).width() > 680)
                $('.menu').css('display', 'block');
            else
                $('.menu').css('display', 'none');
            return false;
        });




        $(document)


            //Delete User
            .on("click", ".userDelete", function(ev) {
                var id = $(this).parents("[data-role=collapsible]").first().attr("data-user-id");
                that.destroyUser(id);
                location.reload();
            })

            //Restore User
            .on("click", ".userRestore", function(ev) {
                var id = $(this).parents("[data-role=collapsible]").first().attr("data-user-id");
                that.restoreUser(id);
                location.reload();
            })

            // Destroy Task
            .on("click", ".taskDelete", function(ev) {
                var id = $(this).parents("[data-role=collapsible]").first().attr("data-task-id");
                that.destroyTask(id);

            })
            // Done Task
            .on("click", ".taskDone", function(ev) {
                var id = $(this).parents("[data-role=collapsible]").first().attr("data-task-id");
                that.doneTask(id);
                location.reload();
            })

            // Redo Task
            .on("click", ".taskRedo", function(ev) {
                var id = $(this).parents("[data-role=collapsible]").first().attr("data-task-id");
                that.redoTask(id);
                location.reload();
            })
            //Update Task
            .on("click", ".taskEdit", function(ev) {
                var id = $(this).parents("[data-role=collapsible]").first().attr("data-task-id");
                that.updateTask(id);
                location.reload();
            })

            // Destroy List
            .on("click", ".listDelete", function(ev) {
                var id = $(this).parents("[data-role=collapsible]").first().attr("data-list-id");
                that.destroyList(id);
                location.reload();
            })

            // Update List
            .on("click", ".listUpdate", function(ev) {
                var id = $(this).parents("[data-role=collapsible]").first().attr("data-list-id");
                that.updateList(id);
                location.reload();
            })




            // Store Task
            .on("submit", "#form-task-add", function(ev) {
                ev.preventDefault();
                var form = $(this);
                that.storeTask(form);
                location.reload();
            })



        ;
    },
    destroy: function(modelName, id) {
        var that = this;

        return $.ajax({
            type: "DELETE",
            url: baseUrl + modelName + "/" + id,
            beforeSend: function(jqXHR) {
                $.mobile.loading("show");
            },
            error: function(jqXHR, textStatus, errorThrown) {
                that.logErrors(jqXHR, textStatus, errorThrown);
            },
            complete: function() {
                $.mobile.loading("hide");
            }
        });
    },
    destroyTask: function(id) {
        var taskElement = $("[data-task-id=" + id + "]");
        taskElement.hide();
        console.log("delete")
        this.destroy("taskDel", id)
            .fail(function() {
                taskElement.show();
                console.log("delete");
            })
        ;
    },
    destroyList: function(id) {
        var listElement = $("[data-list-id=" + id + "]");
        listElement.hide();
        this.destroy("listDel", id)
            .fail(function() {
                listElement.show();
            })
        ;
    },

    destroyUser: function(id) {
        var userElement = $("[data-user-id=" + id + "]");
        userElement.hide();
        this.destroy("userDel", id)
            .fail(function() {
                userElement.show();
            })
        ;
    },

    restore: function(modelName, id) {
        var that = this;

        return $.ajax({
            type: "GET",
            url: baseUrl + modelName + "/" + id,
            beforeSend: function(jqXHR) {
                $.mobile.loading("show");
            },
            error: function(jqXHR, textStatus, errorThrown) {
                that.logErrors(jqXHR, textStatus, errorThrown);
            },
            complete: function() {
                $.mobile.loading("hide");
            }
        });
    },
    restoreUser: function(id) {
        var userElement = $("[data-user-id=" + id + "]");
        userElement.hide();
        this.restore("userRestore", id)
            .fail(function() {
                userElement.show();
            })
        ;
    },
    update: function(modelName, id) {
        var that = this;
        console.log("Button for post " + id + " clicked.");
        return $.ajax({
            type: "GET",
            url: baseUrl + modelName + "/" + id


        });

    },

    updateTask: function(id) {
        this.update("taskEdit", id);
    },

    updateList: function(id) {
        this.update("listEdit", id);
        this.update("listEdit", id);
    },




    finish: function(modelName, id) {
        var that = this;
        return $.ajax({
            type: "PUT",
            url: baseUrl + modelName + "/" + id


        });

    },
    doneTask: function(id){
        this.finish("taskDone", id);
    },
    redoTask: function(id){
        this.finish("taskRedo", id);
    },

    storeTask: function(form) {
        var that = this;
        var nameInput = form.find("input[name=task-name]");
        var dueInput = form.fin
        var task = new Task();
        task.setName(nameInput.val().trim());
        that.store("task", task)
            .done(function(task) {
                nameInput.val("");
                that.renderTask(task);
            })
        ;
    },

    templates: {
        user: '<div data-role="collapsible" data-collapsed="false" data-user-id="${user.id}">' +
            '<p><span class="bold">${user.username}</span></p><span id="mail_admin">${user.email}</span>' +
            '<br/><button class="ui-btn ui-corner-all ui-btn-inline ui-btn-icon-left ui-icon-delete userDelete ui-mini">Verwijder: ${user.username}</button>' +
            '</div>',
        restoreuser: '<div data-role="collapsible" data-collapsed="false" data-user-id="${user.id}">' +
            '<p>${user.username}</p>' +
            '<button class="ui-btn ui-corner-all ui-btn-inline ui-btn-icon-left ui-icon-delete userRestore ui-mini">Restore: ${user.username}</button>' +
            '</div>',
        ddlUser: '<div data-role="collapsible" data-collapsed="false" data-user-id="${user.id}">' +
            '<p>${user.username}</p>' +
            '<button id="btn-toevoeg"></button>' +
            '</div>',
        friend: '<div data-role="collapsible" data-collapsed="false" data-user-id="${friend.id}">' +
            '<p>${friend.friends_id}</p>' +
            '<p>${friend.username}</p>' +
            '</div>',
        finished: '<div class="taken" data-role="collapsible" data-collapsed="false" data-task-id="${task.id}">' +
            '<button class="taskRedo"></button>'+
            '<p><span class="bold">${task.name}</span></p><span class="taakdue">${task.due_at}</span>'+
            '<span class="cursief">${task.tags}</span>'+
            '<div class="${task.prioriteit}"></div>'+
            '<div class="btn-taak">'+
            '<button class="taskDelete"></button>' +
            '<a class="taskEdit" href="/nmdad-ii.arteveldehogeschool.be/public/api/taskEdit/${task.id}"><button class="taskEdit_btn"></button></a>' +
            '</div>'+
            '<ul class="inLijst" >' +
            '</ul>' +
            '</div>',
        task: '<div class="taken" data-role="collapsible" data-collapsed="false" data-task-id="${task.id}">' +
            '<button class="taskDone"></button>'+
            '<p><span class="bold">${task.name}</span></p><span class="taakdue">${task.due_at}</span>'+
            '<span class="cursief">${task.tags}</span>'+
            '<div class="${task.prioriteit}"></div>'+
            '<div class="btn-taak">'+
            '<button class="taskDelete"></button>' +
            '<a class="taskEdit" href="/nmdad-ii.arteveldehogeschool.be/public/api/taskEdit/${task.id}"><button class="taskEdit_btn"></button></a>' +
            '</div>'+
            '<ul>' +
            '</ul>' +
            '</div>',
        listDel: '<div id="lijst" data-role="collapsible" data-collapsed="false" data-list-id="${list.id}">' +
            '<li><h4>${list.name}</h4></li>' +
            '<div class="btn-list">'+
            '<button class="listDelete"></button>'+
            '<a class=" listUpdate" href="/nmdad-ii.arteveldehogeschool.be/public/api/listEdit/${list.id}"><button class="listUpdate_btn"></button></a>' +
            '</div>'+
            '<p>Taken in deze lijst:</p>' +
            '<ul class="lijstTodos">' +
            '</ul>' +
            '</div>',
        list: '<div id="lijst" data-role="collapsible" data-collapsed="false" data-list-id="${list.id}">' +
            '<li><h4>${list.name}</h4></li>' +
            '<div class="btn-list">'+
            '<a class=" listUpdate" href="/nmdad-ii.arteveldehogeschool.be/public/api/listEdit/${list.id}"><button class="listUpdate_btn"></button></a>' +
            '</div>'+
            '<p>Taken in deze lijst:</p>' +
            '<ul class="lijstTodos">' +
            '</ul>' +
            '</div>',
        tasklist:'<p><span class="italic">• ${task.name}</span><span class="taakdue2">${task.due_at}</span></p>',
        taskAdmin:'<p><span class="bold">${task.name}</span></p><span class="taakdue">${task.due_at}</span>'+
            '<div class="${task.prioriteit}"></div>'


    }
}

App.init();

