@extends('layouts.master')

@section('content')
<div data-role="page" id="page-share">
    <div data-role="header">
        <div class="ui-block-a" id="header">
            <a href=""><img class="logo" src="styles/images/logoBizzi.png"/></a>
           <!-- <ul id="search-head" data-role="listview" data-filter="true" data-filter-placeholder="&hellip;" data-inset="true" data-split-icon="delete"></ul>-->
        </div>
        <div id="head-btn">
            <div class="" id="profiel">
                @if ( Auth::check() )
                <div class="foto" style="background: url('<?php echo Auth::user()->getProfilePictureUrl() ?>') no-repeat;"> </div>
                <p>{{ Auth::user()->username}}</p>
                @else

                @endif
                {{ HTML::linkRoute('user.logout', 'AFMELDEN', [], [
                'id'        => 'btn-afmeld',
                'class'     => 'ui-btn ui-btn-inline',
                'data-ajax' => 'false',
                ]) }}
            </div>
        </div>
    </div>
    @include('navigation', ['pageActive' => 'page-friends'])
    <div data-role="content" role="main" class="ui-content" id="main-container">
        {{ Form::open(array('action' => 'ShareController@share')) }}
        <h1>Lijsten delen</h1>
        <div id="page-list">
            <h3>Lijsten</h3>
            @foreach ($lists as $task)
            @if($task->user_id == Auth::user()->id && $task->deleted_at == null)

                <section class='list-info clearfix'>
                    <div><h5>{{ $task->name }}</h5>






                        {{Form::checkbox('list_selected_id[]', $task->id, false)}}
                        <input type="hidden" name="task_hidden" value="{{ $task->id }}"/>

                    </div>
                </section>

            @endif
            @endforeach
        </div>

        <?php $getUser = Auth::user()->id; ?>
        <div id="page-friends">
            <h3>Vrienden</h3>
            @foreach($friends as $friend)
            @if ($friend->defriended_at == '0000-00-00 00:00:00' && $friend->accepted_at != '0000-00-00 00:00:00')
            @if($friend->user_id == $getUser)
            <div class="icon red clearfix">

                <section class='friend-info clearfix'>
                    <h5>{{ DB::select('select username from users where id = ?', array($friend->friends_id))[0]->username }}</h5>
                    {{Form::checkbox('friends_id[]', $friend->friends_id, false)}}
                    <input type="hidden" name="friend_hidden" value="{{ $friend->friends_id }}"/>
                </section>
            </div>
            @endif

            @if($friend->friends_id == $getUser)
            <div class="icon red clearfix">
                <section class='friend-info clearfix'>
                    <h5>{{ DB::select('select username from users where id = ?', array($friend->user_id))[0]->username }}</h5>
                    {{Form::checkbox('friends_id[]', $friend->user_id, false)}}
                    <input type="hidden" name="friend_hidden" value="{{ $friend->user_id }}"/>
                </section>
            </div>
            @endif
            @endif
            @endforeach
        </div>
        <div class="ui-input-btn ui-btn allBtn">
            Delen
            {{ Form::submit('Delen', [
            'data-enhanced' => 'true'
            ]), PHP_EOL }}
        </div>

        <div class="ui-input-btn ui-btn allBtn_Anu">
            {{ HTML::linkRoute('lists', 'Annuleren', [], [
            'data-ajax' => 'false',
            ]) }}
        </div>
        {{ Form::close() }}
    </div>
</div>
@stop





