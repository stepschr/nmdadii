@extends('layouts.master')
@section('head')
    {{ HTML::script('scripts/utilities.js') }}
    {{ HTML::script('scripts/models/Pomodoro.js') }}
    {{ HTML::script('scripts/models/Label.js') }}
    {{ HTML::script('scripts/models/Lists.js') }}
    {{ HTML::script('scripts/models/Task.js') }}
    {{ HTML::script('scripts/app.js') }}
@stop

@section('content')

    <div id="page-tasks" data-role="page">

        <div data-role="header">
            <div class="ui-block-a" id="header">
                <a href=""><img class="logo" src="styles/images/logoBizzi.png"/></a>
                <!--<ul id="search-head" data-role="listview" data-filter="true" data-filter-placeholder="&hellip;" data-inset="true" data-split-icon="delete">
                <div id="ddlUsers">

                </div>
                </ul>-->
            </div>
            <div id="head-btn">
                <div class="" id="profiel">
                    @if ( Auth::check() )
                    <div class="foto" style="background: url('<?php echo Auth::user()->getProfilePictureUrl() ?>') no-repeat;"> </div>
                    <p>{{ Auth::user()->username}}</p>
                    @else

                    @endif
                    {{ HTML::linkRoute('user.logout', 'AFMELDEN', [], [
                    'id'        => 'btn-afmeld',
                    'class'     => 'ui-btn ui-btn-inline',
                    'data-ajax' => 'false',
                    ]) }}
                </div>
            </div>
        </div>
        @include('navigation', ['pageActive' => 'page-tasks'])
        <div data-role="content" role="main" class="ui-content" id="main-container">

            <h1>Komende taken</h1>
            <?php
            $list = DB::table('lists')->where('user_id', Auth::user()->id)->whereNull('deleted_at')->get(); ?>

            @if(count($list)>0)

            @foreach($list as $lijsten)

            <div id="tasks"></div>


            <div id="finishedtasks"></div>
            @endforeach
            <div class="toevoeg">
                {{ HTML::linkRoute('task.create', '', [] , [
                'id'    => 'btn-toevoeg',
                'class'     => 'ui-btn ui-btn-inline',
                ]) }}
                Nieuwe taak toevoegen
            </div>

            @else

            <h3>Om taken toe te voegen moet je eerst een lijst aanmaken</h3>
            @endif


        </div><!-- /content -->
        <div class="wrapper" id="footer-wrapper">
            <footer class="container" id="footer" role="footer">
                <p>© Stephanie Schroé in opdracht van Arteveldehogeschool | 2MMP | 2013 -2014</p>
            </footer>
        </div>
    </div><!-- /page -->
<div id="page-task-create" data-transition="slide" data-role="page" data-dialog="true" data-close-btn="right">
    <div data-role="header">
        <h4>Taak Toevoegen</h4>
    </div>
    <div role="main" class="ui-content">

            {{ Form::open([
            'route' => 'task.store',
            'data-ajax' => 'false',
            ]), PHP_EOL }}
            <fieldset>
                {{ Form::label('name', "Naam" . ':'), PHP_EOL }}
                <div class="ui-input-text ui-body-inherit">
                    {{ Form::text('name', '', [
                    'placeholder' => "Naam",
                    'data-enhanced' => 'true',
                    ]), PHP_EOL }}
                </div>

                {{ Form::label('due_at', "Deadline" . ':'), PHP_EOL }}
                <div class="ui-input-text ui-body-inherit">
                    {{ Form::text('due_at', '',[
                    'placeholder' => "Deadline: YYYY-MM-DD HH:MM:SS",
                    'data-enhanced' => 'true',
                    'data-role' => 'date',
                    ]), PHP_EOL }}

                </div>
                <div class="tags">
                {{ Form::label('tags', "Tag" . ':', ['class' => '']), PHP_EOL }}

                    {{ Form::select('tags', Tag::lists('name', 'class')) }}
                </div>
                <div class="listsDrop">
                {{ Form::label('lists_id', "Lijst" . ':'), PHP_EOL }}

                    {{ Form::select('lists_id', Lists::where('user_id', '=', Auth::user()->id)->lists('name', 'id')) }}
                </div>
                <div class="prior">
                {{ Form::label('prioriteit', "Prioriteit" . ':', ['class' => '']), PHP_EOL }}

                    {{ Form::select('prioriteit', Prioriteit::lists('name', 'class')) }}
                </div>



            </fieldset>

            <div class="ui-input-btn ui-btn allBtn">
                Toevoegen
                {{ Form::submit('Toevoegen', [
                'data-enhanced' => 'true'
                ]), PHP_EOL }}
            </div>

            {{ Form::close(), PHP_EOL }}

    </div>

</div><!-- /page -->
<div id="taskEdit/${task.id}" data-role="page" data-dialog="true" data-close-btn="right">
    <div data-role="header">
        <h1>Taak bewerken</h1>
    </div>


</div><!-- /page -->





<div id="page-instellingen" data-role="page">
    <div class="wrapper" id="header-wrapper" data-role="header">
        <header class="container" id="header" role="header">
            <div class="row ui-block-a">

                <img class="logo" src="styles/images/logoBizzi.png"/>
                <!--<ul id="zoekbalk" data-role="listview" data-filter="true" data-filter-placeholder="&hellip;" data-inset="true" data-split-icon="delete"></ul>-->
            </div>
        </header>

    </div>
        <div id="head-btn">
            <div class="" id="profiel">
                @if ( Auth::check() )
                <div class="foto" style="background: url('<?php echo Auth::user()->getProfilePictureUrl() ?>') no-repeat;"> </div>
                <p>{{ Auth::user()->username}}</p>
                @else

                @endif
                {{ HTML::linkRoute('user.logout', 'AFMELDEN', [], [
                'id'        => 'btn-afmeld',
                'class'     => 'ui-btn ui-btn-inline',
                'data-ajax' => 'false',
                ]) }}
            </div>
        </div>


    @include('navigation', ['pageActive' => 'page-instellingen'])
<div class="wrapper" id="main-wrapper">
    <div data-role="content" role="main" class="ui-content" id="main-container">
            <div class="" id="inhoud">
                <h1>Instellingen</h1>
                {{ Form::model(Auth::user(), array('route' => array('user.profile_picture'), 'data-ajax'=>'false', 'files'=>true)) }}
                <fieldset>
                    <div class="formleft col col-sc1-12 col-sc2-12 col-sc3-12 col-sc4-6 col-sc5-6">
                        <legend class="ui-hidden-accessible">Aanmeldgegevens</legend>

                        {{ Form::label('email', 'Email', ['class' => '']) }}
                        {{ Form::email('email', null, array('class'=>"ui-input-text ui-body-inherit", 'disabled')); }}
                        <div class="error">{{$errors->first('email');}}</div>

                        {{ Form::label('username', 'Gebruikersnaam', ['class' => '']) }}
                        {{ Form::text('username', null, array('class'=>"ui-input-text ui-body-inherit")); }}
                        <div class="error">{{$errors->first('username');}}</div>

                        {{ Form::label('password', 'Wachtwoord', ['class' => '']) }}
                        {{ Form::password('password', '', array('class'=>"ui-input-text ui-body-inherit")); }}
                        <div class="error">{{$errors->first('password');}}</div>

                        {{ Form::label('password_repeat', 'Herhaal wachtwoord', ['class' => '']) }}
                        {{ Form::password('password_repeat', '', array('class'=>"ui-input-text ui-body-inherit")); }}
                        <div class="error">{{$errors->first('password_repeat');}}</div>



                        <div class="ui-input-btn ui-btn allBtn">
                            Wijzigen
                            {{ Form::submit('Wijzigen', [
                            'data-enhanced' => 'true'
                            ]), PHP_EOL }}
                        </div>
                    </div>
                    <div class="formright col col-sc1-12 col-sc2-12 col-sc3-12 col-sc4-6 col-sc5-6" >
                        <!-- <div class="foto_aanpas" style="background: url('styles/images/pic.jpg') no-repeat;">-->
                        <div class="foto_aanpas" style="background: url('<?php echo Auth::user()->getProfilePictureUrl(); ?>') no-repeat;">
                            <div class="foto_upload">{{ Form::file('profile_picture') }}</div>
                            <div class="error">{{$errors->first('profile_picture');}}</div>
                        <div>
                    </div>

                </fieldset>
                {{ Form::close() }}




                </fieldset>
            </div>

  </div><!-- /page -->
</div>
    <div class="wrapper" id="footer-wrapper">
        <footer class="container" id="footer" role="footer">
            <p>© Stephanie Schroé in opdracht van Arteveldehogeschool | 2MMP | 2013 -2014</p>
        </footer>
    </div>
</div>

@stop

