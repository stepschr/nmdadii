@extends('layouts.master')
@section('head')
{{ HTML::script('scripts/utilities.js') }}
{{ HTML::script('scripts/models/Pomodoro.js') }}
{{ HTML::script('scripts/models/Label.js') }}
{{ HTML::script('scripts/models/Lists.js') }}
{{ HTML::script('scripts/models/Task.js') }}
{{ HTML::script('scripts/app.js') }}
@stop
@section('content')

<div id="page-lists" data-role="page" data-url="{{URL::route('lists')}}">
    <div data-role="header">
        <div class="ui-block-a" id="header">
            <a href=""><img class="logo" src="styles/images/logoBizzi.png"/></a>
            <!--  <ul id="search-head" data-role="listview" data-filter="true" data-filter-placeholder="&hellip;" data-inset="true" data-split-icon="delete"></ul>-->
        </div>
        <div id="head-btn">
            <div class="" id="profiel">
                @if ( Auth::check() )
                <div class="foto" style="background: url('<?php echo Auth::user()->getProfilePictureUrl() ?>') no-repeat;"> </div>
                <p>{{ Auth::user()->username}}</p>
                @else

                @endif
                {{ HTML::linkRoute('user.logout', 'AFMELDEN', [], [
                'id'        => 'btn-afmeld',
                'class'     => 'ui-btn ui-btn-inline',
                'data-ajax' => 'false',
                ]) }}
            </div>
        </div>
    </div>

    @include('navigation', ['pageActive' => 'page-lists'])
    <div data-role="content" role="main" class="ui-content" id="main-container">
        <div class="ui-grid-b">

            <h1>Lijsten:</h1>
            <p>Indien u een lijst wenst te verwijderen moet u zorgen dat alle taken uit deze lijst verwijdert zijn.</p>
            <ul id="lijsten">

            </ul>



            {{ Form::open([
            'route' => 'lijst.store',
            'data-ajax' => 'false',
            ]), PHP_EOL }}

            <div class="ui-input-btn ui-btn ui-btn-inline ui-btn-b">

                {{ Form::submit('Toevoegen', [
                'id'    => 'btn-toevoeg-lijst',
                'data-enhanced' => 'true'
                ]), PHP_EOL }}
            </div>

            <fieldset>
                {{ Form::label('name', "Naam" . ':', ['class' => 'ui-hidden-accessible']), PHP_EOL }}
                <div class="input-list ui-input-text ui-body-inherit{{{ $errors->has('email') ? ' error' : '' }}}">
                    {{ Form::text('name', '', [
                    'placeholder' => "Lijst toevoegen",
                    'data-enhanced' => 'true',
                    ]), PHP_EOL }}
                    @if ($errors->has('name'))
                    {{ $errors->first('name', '<small class="ui-bar">:message</small>') }}
                    @endif
                </div>
            </fieldset>


            {{ Form::close(), PHP_EOL }}


            <div id="shared">
                <h3>Gedeelde lijsten</h3>
                <?php
                $list = DB::table('lists')->where('user_id', Auth::user()->id)->whereNull('deleted_at')->get();
                $hasfriends = DB::table('friends')
                    ->where('accepted_at', '!=', '0000-00-00 00:00:00')
                    ->where('defriended_at', '0000-00-00 00:00:00')
                    ->where(function($query)
                    {
                        $query->where('user_id', Auth::user()->id)
                            ->orWhere('friends_id', Auth::user()->id);
                    })
                    ->whereNull('deleted_at')->get();
                ?>

                @if((count($list)>0) && (count($hasfriends)>0))
                @foreach ($lists as $list)
                <?php $string = '"'.Auth::user()->id.'"'; ?>
                @if(($list->user_id == Auth::user()->id || strpos($list->share, $string)!= null) && $list->deleted_at == null)

                <div class="lijstGed">

                    @if(strpos($list->share, $string)!= null)
                    Lijst:<span class="bold"> {{ $list->name }}</span>
                    <?php $tasks= DB::table('tasks')->where('lists_id', $list->id)->whereNull('deleted_at')->get();?>
                    <p>Taken in deze lijst:</p>
                    <ul>
                        @if(count($tasks)>0)
                        @foreach($tasks as $task)


                        <div class="taken" data-role="collapsible" data-collapsed="false" data-task-id="{{$task->id}}">
                            @if($task->finished_at=='0000-00-00 00:00:00')
                            <button class="taskDone"></button>
                            @else
                            <button class="taskRedo"></button>
                            @endif
                            <p><span class="bold">{{$task->name}}</span></p><span class="taakdue">{{$task->due_at}}</span>
                            <span class="cursief">{{$task->tags}}</span>
                            <div class="{{$task->prioriteit}}"></div>
                            <div class="btn-taak2">
                                <button class="taskDelete"></button>
                            </div>
                            <ul class="inLijst" ></ul>
                        </div>



                        @endforeach
                        @else
                        <li>Er zijn geen taken in deze lijst</li>
                        @endif
                    </ul>
                    <?php $user =  DB::table('users')->where('id', $list->user_id)->first();  ?>
                    <p>Deler: <span class="italic"> {{$user->username}}</span></p>
                    @endif

                    @if($list->share != null)
                    @if(($list->user_id == Auth::user()->id))
                    Lijst:<span class="bold"> {{ $list->name }}</span>

                    <?php $tasks= DB::table('tasks')->where('lists_id', $list->id)->whereNull('deleted_at')->get();?>

                    <p>Taken in deze lijst:</p>
                    <ul>
                        @if(count($tasks)>0)
                        @foreach($tasks as $task)

                        <div class="taken" data-role="collapsible" data-collapsed="false" data-task-id="{{$task->id}}">
                            @if($task->finished_at=='0000-00-00 00:00:00')
                            <button class="taskDone"></button>
                            @else
                            <button class="taskRedo"></button>
                            @endif
                            <p><span class="bold">{{$task->name}}</span></p><span class="taakdue">{{$task->due_at}}</span>
                            <span class="cursief">{{$task->tags}}</span>
                            <div class="{{$task->prioriteit}}"></div>
                            <div class="btn-taak2">
                                <button class="taskDelete"></button>
                            </div>
                            <ul class="inLijst" ></ul>
                        </div>


                        @endforeach
                        @else
                        <li>Er zijn geen taken in deze lijst</li>
                        @endif
                    </ul>

                    <?php $share = $list->share;
                    $id = explode('"', $share);
                    ?>
                    <?php $vriend = DB::table('users')->where('id', (int)$id[1])->first(); ?>
                    <p>Gedeeld met: <span class="italic"> {{$vriend->username}}</span></p>
                    @else

                    @endif
                    @endif



                </div>

                @endif
                @endforeach


                <p class="link">Deel een lijst</p>
                <a class="listShare" href="/nmdad-ii.arteveldehogeschool.be/public/listShare"><button class="listShare_btn"></button></a>
                @else
                <p>Om lijsten te kunnen delen moet je eerst lijsten hebben. Daarnaast moet je ook vrienden hebben waarmee je ze kunt delen.</p>
                @endif
            </div>

        </div>
    </div>
    <div class="wrapper" id="footer-wrapper">
        <footer class="container" id="footer" role="footer">
            <p>© Stephanie Schroé in opdracht van Arteveldehogeschool | 2MMP | 2013 -2014</p>
        </footer>
    </div>
</div><!-- /page -->
@stop