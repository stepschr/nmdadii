@extends('layouts.master')
@section('head')
{{ HTML::script('scripts/utilities.js') }}
{{ HTML::script('scripts/models/Pomodoro.js') }}
{{ HTML::script('scripts/models/Label.js') }}
{{ HTML::script('scripts/models/Lists.js') }}
{{ HTML::script('scripts/models/Task.js') }}
{{ HTML::script('scripts/app.js') }}
@stop

@section('content')
<div data-role="header">
    <div class="ui-block-a" id="header">
        <a href=""><img class="logo" src="../../styles/images/logoBizzi.png"/></a>
       <!-- <ul id="search-head" data-role="listview" data-filter="true" data-filter-placeholder="&hellip;" data-inset="true" data-split-icon="delete"></ul>-->
    </div>
    <div id="head-btn">
        <div class="" id="profiel">
            @if ( Auth::check() )
            <div class="foto" style="background: url('../../<?php echo Auth::user()->getProfilePictureUrl() ?>') no-repeat;"> </div>
            <p>{{ Auth::user()->username}}</p>
            @else

            @endif
            {{ HTML::linkRoute('user.logout', 'AFMELDEN', [], [
            'id'        => 'btn-afmeld',
            'class'     => 'ui-btn ui-btn-inline',
            'data-ajax' => 'false',
            ]) }}
        </div>
    </div>
</div>
<div class="ui-grid-b">
    <div class="ui-block-a"></div>
    <div class="ui-block-b">
        <h1>Taak Bewerken </h1>
        @if ($errors->any())
        <div class="ui-corner-all">
            <div class="ui-bar ui-bar-a">
                <h3>Kan Taak niet bewerken.</h3>
            </div>
            <div class="ui-body ui-body-a">
                <p>Controleer de in <span class="error">rood</span> aangeduide velden.</p>
                <ul>
                    @foreach ($errors->all('<li>:message</li>' . PHP_EOL) as $message)
                    {{ $message }}
                    @endforeach
                </ul>
            </div>
        </div>

        @endif

        {{ Form::open(['data-ajax' => 'false', 'method' => 'post', 'action' => array('TaskController@update', $id)]) }}
        <fieldset>
            {{ Form::label('name', "Naam" . ':'), PHP_EOL }}
            <div class="ui-input-text ui-body-inherit{{{ $errors->has('email') ? ' error' : '' }}}">
                {{ Form::text('name', $task->name, [
                'placeholder' => "Naam",
                'data-enhanced' => 'true',
                ]), PHP_EOL }}
                @if ($errors->has('name'))
                {{ $errors->first('name', '<small class="ui-bar">:message</small>') }}
                @endif
            </div>



            {{ Form::label('due_at', "Deadline" . ':'), PHP_EOL }}
            <div class="ui-input-text ui-body-inherit{{{ $errors->has('password') ? ' error' : '' }}}">
                {{ Form::text('due_at', $task->due_at,[
                'placeholder' => "Deadline: YYYY-MM-DD HH:MM:SS",
                'data-enhanced' => 'true',
                ]), PHP_EOL }}

            </div>

            <div class="tags">
            {{ Form::label('tags', "Tags" . ':'), PHP_EOL }}

                {{ Form::select('tags', Tag::lists('name', 'class'), $task->tags) }}
            </div>
            <div class="listsDrop">
            {{ Form::label('lists_id', "Lijst" . ':'), PHP_EOL }}

                {{ Form::select('lists_id', Lists::where('user_id', '=', Auth::user()->id)->lists('name', 'id'), $task->lists_id) }}
            </div>
            <div class="prior">
            {{ Form::label('prioriteit', "Prioriteit" . ':'), PHP_EOL }}

                {{ Form::select('prioriteit', Prioriteit::lists('name', 'class'), $task->prioriteit) }}
            </div>

        </fieldset>

        <div class="ui-input-btn ui-btn ui-btn-inline allBtn">
            Bijwerken
            {{ Form::submit('Bewerken', [
            'data-enhanced' => 'true'
            ]), PHP_EOL }}
        </div>

        <div class="ui-input-btn ui-btn allBtn_Anu">
            {{ HTML::linkRoute('tasks', 'Annuleren', [], [
            'data-ajax' => 'false',
            ]) }}
        </div>

        {{ Form::close(), PHP_EOL }}
</div>
@stop