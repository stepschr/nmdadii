@extends('layouts.master')
@section('head')
{{ HTML::script('scripts/utilities.js') }}
{{ HTML::script('scripts/models/Pomodoro.js') }}
{{ HTML::script('scripts/models/Label.js') }}
{{ HTML::script('scripts/models/Lists.js') }}
{{ HTML::script('scripts/models/Task.js') }}
{{ HTML::script('scripts/app.js') }}
@stop
@section('content')

<div data-role="page" id="page-vrienden" data-url="{{URL::route('friends')}}">
    <div data-role="header">
        <div class="ui-block-a" id="header">
            <a href=""><img class="logo" src="styles/images/logoBizzi.png"/></a>
          <!--  <ul id="search-head" data-role="listview" data-filter="true" data-filter-placeholder="&hellip;" data-inset="true" data-split-icon="delete"></ul>-->
        </div>
        <div id="head-btn">
            <div class="" id="profiel">
                @if ( Auth::check() )
                <div class="foto" style="background: url('<?php echo Auth::user()->getProfilePictureUrl() ?>') no-repeat;"> </div>
                <p>{{ Auth::user()->username}}</p>
                @else

                @endif
                {{ HTML::linkRoute('user.logout', 'AFMELDEN', [], [
                'id'        => 'btn-afmeld',
                'class'     => 'ui-btn ui-btn-inline',
                'data-ajax' => 'false',
                ]) }}
            </div>
        </div>
    </div>
    @include('navigation', ['pageActive' => 'page-vrienden'])
    <div data-role="content" role="main" class="ui-content" id="main-container">
        <div class="title">
            <h1>Uw vriendenlijst</h1>
        </div>


        <?php $arrayFriends = array(); $getUser = Auth::user()->id;?>


        @foreach($friends as $friend_accept)
        @if($friend_accept->defriended_at == '0000-00-00 00:00:00' && $friend_accept->rejected_at == '0000-00-00 00:00:00' && !in_array($friend_accept->friends_id, $arrayFriends) && !in_array($friend_accept->user_id, $arrayFriends))

        @if($friend_accept->friends_id == $getUser && $friend_accept->accepted_at == '0000-00-00 00:00:00')

        <?php
        if(!in_array($friend_accept->friends_id, $arrayFriends) && $friend_accept->friends_id!=$getUser){
            array_push($arrayFriends, $friend_accept->friends_id);
        }elseif(!in_array($friend_accept->user_id, $arrayFriends) && $friend_accept->user_id!=$getUser){
            array_push($arrayFriends, $friend_accept->user_id);
        }
        $pending = '<strong><small>wil je vriend zijn</small></strong><input type="hidden" name="reject" value="'. $friend_accept->id .'"/><input type="hidden" name="update" value="'. $friend_accept->id .'"/><input type="submit" id="friend_add" name="updateFriend"/><input type="submit" id="friend_reject" name="rejectFriend"/>';
        ?>

        <div class="icon red clearfix">
            <section class='friend-info clearfix'>
                <h3>{{ DB::select('select username from users where id = ?', array($friend_accept->user_id))[0]->username }}</h3>
                {{Form::open()}} {{$pending}} {{Form::close()}}
            </section>
        </div>



        @endif
        @endif
        @endforeach



        @foreach($friends as $friend)

        @if($friend->defriended_at == '0000-00-00 00:00:00' && $friend->rejected_at == '0000-00-00 00:00:00' && !in_array($friend->friends_id, $arrayFriends) && !in_array($friend->user_id, $arrayFriends))

        @if($friend->friends_id == $getUser || $friend->user_id == $getUser)

        <?php
        $pending ="";
        // !in_array($friend->friend_id, $arrayFriends))
        if(!in_array($friend->friends_id, $arrayFriends) && $friend->friends_id!=$getUser){
            array_push($arrayFriends, $friend->friends_id);
        }elseif(!in_array($friend->user_id, $arrayFriends) && $friend->user_id!=$getUser){
            array_push($arrayFriends, $friend->user_id);
        }

        if($friend->accepted_at != '0000-00-00 00:00:00') {
            $pending = '<strong><small>Geaccepteerd</small></strong><div id="friend_acc"></div><input type="hidden" name="delete" value="'. $friend->id .'"/><input type="submit" id="friend_reject" name="deleteFriend"/>';
        } else {
            if($getUser == $friend->user_id) {
                $pending = '<strong><small>Wachten op antwoord</small></strong><div id="friend_pending"></div><input type="hidden" name="reject" value="'. $friend->id .'"/><input type="submit" id="friend_reject" name="rejectFriend"/>';
            } else {
                $pending = '<strong><small>Aanvraag</small></strong><input type="hidden" name="reject" value="'. $friend->id .'"/><input type="hidden" name="update" value="'. $friend->id .'"/><input type="submit" id="friend_acc" name="updateFriend"/><input type="submit" id="friend_reject" name="rejectFriend"/>';
            }
        }
        ?>

        @if($friend->user_id == $getUser)
        <div class="icon orange clearfix">
            <section class='friend-info clearfix'>
                <h3>{{ DB::select('select username from users where id = ?', array($friend->friends_id))[0]->username }}</h3>

                {{Form::open()}} {{$pending}} {{Form::close()}}
            </section>
        </div>
        @endif
        @if($friend->friends_id == $getUser)
        <div class="icon orange clearfix">
            <section class='friend-info clearfix'>
                <h3>{{ DB::select('select username from users where id = ?', array($friend->user_id))[0]->username }}</h3>
                {{Form::open()}} {{$pending}} {{Form::close()}}
            </section>
        </div>
        @endif
        @endif
        @endif
        @endforeach
        <h1>Gebruikerslijst</h1>
        @foreach($users as $user)
        @if(!in_array($user->id, $arrayFriends) && $user->id!=$getUser && $user->is_admin != 1)
        <div class="">


                <h3>{{$user->username}}</h3>
                  {{Form::open()}}
                <input type="hidden" name="add" value="{{$user->id}}"/>
                <input type="submit" id="friend_add" name="addFriend"/>
                {{Form::close()}}

        </div>
        @endif
        @endforeach
    </div>
    <div class="wrapper" id="footer-wrapper">
        <footer class="container" id="footer" role="footer">
            <p>© Stephanie Schroé in opdracht van Arteveldehogeschool | 2MMP | 2013 -2014</p>
        </footer>
    </div>
</div>

@stop





