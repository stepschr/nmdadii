@extends('layouts.master')
<script src="//www.google.com/jsapi"></script>
<script src="chartkick.js"></script>
@section('content')

<h1>Admin Panel</h1>
<h2>Actieve Users:</h2>

<div id="userlist"></div>

<h2>Deleted/Blocked Users:</h2>
<div id="deleteduserlist"></div>

<h2>Alle Todos:</h2>
<div id="alltodo"></div>

<div id="chart-1" style="height: 300px;"></div>

<div class="wrapper" id="footer-wrapper">
    <footer class="container" id="footer" role="footer">
        <p>© Stephanie Schroé in opdracht van Arteveldehogeschool | 2MMP | 2013 -2014</p>
    </footer>
</div>
@stop