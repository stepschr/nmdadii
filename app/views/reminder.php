@extends('layouts.master')

@section('content')

<div data-role="page" id="page-reminder">
    @include('navigation', ['pageActive' => 'page-user'])
    <div data-role="content" role="main" class="ui-content content" id="main-container">
        <div class="title"><a href="#nav-mobile"><span id="nav-logo">-</span></a><h3>Taken</h3></div>
        <?php $string = '"'.Auth::user()->id.'"'; $countReminders=0; ?>
        @foreach ($tasks as $reminder)
        @if(strtotime($reminder->due_at) > strtotime(date("Y-m-d H:i:s")) && $reminder->due_at != "0000-00-00 00:00:00" && ($reminder->user_id == Auth::user()->id || strpos($reminder->share, $string)!= null))

        <?php  $days = strtotime($reminder->due_at) - strtotime(date("Y-m-d")); $days_left = floor($days/3600/24); ?>
        @if($days_left <= 177)
        <div class="reminder {{ DB::select('select name from priorities where id = ?', array($reminder->priority_id))[0]->name }}">
            <p>Nog <strong>{{ $days_left }} {{ $days_left > 1 ? 'dagen' : 'dag' }}</strong> </p>
            <h5>{{ $reminder->name }}</h5>
            <p class="description">{{ $reminder->description }}</p>

            <div class="bar clearfix">
                <span class="span-left">Tags: <a href="#" class="tag">{{ DB::select('select name from categories where id = ?', array($reminder->category_id))[0]->name }}, {{ $reminder->tag_text }}</a></span>
                                <span class="span-right">Gereed:
                                    {{ Form::open(array('url' => '/reminder', 'class' => 'clearfix')) }}
                                       <input type="checkbox"  onClick="this.form.submit()" class="clearfix"/>
                                        <input type="hidden" name="no_reminder" value="full"/>
                                        <input type="hidden" name="reminder_id" value="{{$reminder->id}}"/>
                                    {{ Form::close() }}
                                </span>
            </div>
            <?php $countReminders++; ?>
        </div>
        @endif
        @endif
        @endforeach

        @if($countReminders == 0)
        {{ Form::open(array('url' => '/', 'id' => 'emptyform')) }}
        <script type="text/javascript"> $(document).ready(function(){$("#emptyform").submit();});</script>
        {{ Form::close() }}
        @endif
        </ul>
        @if($countReminders > 0)
        {{ HTML::linkRoute('home', ' Ga verder naar de taken ', [], [
        'class' => 'ui-btn ui-btn-inline ui-btn-icon-right ui-icon-arrow-r',
        'data-ajax' => 'false',
        ]), PHP_EOL }}
        @endif
    </div>
</div>

@overwrite
