<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


Route::get('/', array('as' => 'tasks', 'uses' => 'TaskController@index'))->before('auth');




Route::resource('user', 'UserController', [
    'only' => ['index', 'create', 'store']
]);

Route::post('user/profile_picture', [
    'as' => 'user.profile_picture',
    'uses' => 'UserController@postProfile'
]);


Route::get('user/login', [
    'as' => 'user.login',
    'uses' => 'UserController@login',
]);

Route::get('user/logout', [
    'as'   => 'user.logout',
    function () {
        Auth::logout();

        return Redirect::route('user.index');
    }
])->before('auth');




Route::post('user/auth', [
    'as' => 'user.auth',
    'uses' => 'UserController@auth'
]);



Route::resource('lijst', 'ListsController', [
    'only' => ['index', 'store']
]);


Route::resource('task', 'TaskController', [
    'only' => ['index', 'create', 'store']
]);



//Routes aanpassen zorgde voor veel problemen. (Connectie Javascript)
//Nieuwe routes heb ik wel anders aangepakt.

Route::group(['prefix' => 'api'/*, 'before' => 'auth'*/], function () {
    Route::resource('deletedusers' , 'UserController@getDeletedUserList');
    Route::resource('userDel' , 'UserController@destroy');
    Route::resource('userRestore' , 'UserController@restore');
    Route::resource('alltask' , 'TaskController@getAllTasks');
    Route::resource('tasklijst' , 'TaskController@getTasks');
    Route::resource('finishedlijst' , 'TaskController@getFinished');
    Route::resource('list' , 'ListsController@getLijst');
    Route::resource('getalllists' , 'ListsController@getAllLijsten');
    Route::resource('listDel' , 'ListsController@destroy');
    Route::resource('listEdit' , 'ListsController@edit');
    Route::resource('taskDel' , 'TaskController@destroy');
    Route::resource('taskEdit' , 'TaskController@edit');
    Route::resource('taskDone' , 'TaskController@finished');
    Route::resource('taskRedo' , 'TaskController@unfinished');
    Route::resource('getsharedlists' , 'ShareController@getSharedLists');

    Route::post('taskEdit/{id}', [
        'as' => 'task.update',
        'uses' => 'TaskController@update'
    ]);

    Route::post('listEdit/{id}', [
        'as' => 'list.update',
        'uses' => 'ListsController@update'
    ]);

    Route::get('users', [
        'as' => 'users',
        'uses' => 'UserController@getUsers',
    ]);

    Route::get('allUsers', [
        'as' => 'allUsers',
        'uses' => 'UserController@getAllUsers',
    ]);

    Route::get('listShare/{id}', array('as' => 'list.share', 'uses' => 'ShareController@edit'));


});



Route::get('/list', [
    'as' => 'lists',
    'uses' => 'ListsController@index',
]);


Route::get('/friends', [
    'as' => 'friends',
    'uses' => 'FriendsController@index',
]);

Route::post('/friends', function() {
    if(Input::get('addFriend')) {
        $action = 'addFriend';
    } elseif(Input::get('deleteFriend')) {
        $action = 'deleteFriend';
    } elseif(Input::get('updateFriend')) {
        $action = 'updateFriend';
    } elseif(Input::get('rejectFriend')) {
    $action = 'rejectFriend';
    }

    return App::make('FriendsController')->$action();
});




Route::get('/listShare', array('as' => 'sharelist', 'uses' => 'ShareController@index'))->before('auth');
Route::post('/listShare', array('uses' => 'ShareController@share'))->before('csrf');

Route::get('/user', array('as' => 'reminderIndex', 'uses' => 'HomeController@reminderindex'));
//Route::post('/user', array('uses' => 'HomeController@reminder'))->before('csrf');


Route::get('admin', [
    'as' => 'user.admin',
    'uses' => 'UserController@admin',
]);


