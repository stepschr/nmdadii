<?php

class TagTableSeeder extends DatabaseSeeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {



        // DemoTaak A
        Tag::create([
            'id'    => '1',
            'name'    => 'Privé',
            'class'   => 'privé'

        ]);

        Tag::create([
            'id'    => '2',
            'name'    => 'Werk',
            'class'   => 'werk'

        ]);

        Tag::create([
            'id'    => '3',
            'name'    => 'School',
            'class'   => 'school'

        ]);




    }

}