<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Stephanie Schroé
 * @copyright  Copyright © 2014 Artevelde University College Ghent
 */

class ListsController extends \BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    protected $layout = 'layouts.master';

    public function index()
    {

           $this->layout->content = View::make('list', array(
            'users' => DB::table('users')->get(),
            'lists' => DB::table('lists')->get(),
            'friends' => DB::table('friends')->get()

        ));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $rules = [
            'name'            => 'required'
        ];

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->passes()) {
            $lists = new Lists(Input::all());
            $lists->user()->associate(Auth::user());
            $lists->save();

            return Redirect::route('lists');

            //
        } else {

            return Redirect::route('lijst.index') // Zie: $ php artisan routes
                ->withInput()             // Vul het formulier opnieuw in met de Input.
                ->withErrors($validator); // Maakt $errors in View.
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {

        $lists = Lists::findOrFail($id);
        return View::make('listEdit', ['list' => $lists, 'id' => $id]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {


            $lists = Lists::find($id);
            $lists->name = Input::get('name');
            $lists->save();
            return Redirect::route('lists');

    }

    public function getLijst(){

        $lists = Lists::where('user_id', '=', Auth::user()->id)->get();

        $lists->load('Tasks');
       // die("die");
       // $tasks = Task::where('lists_id', '=', Lists::where('user_id', '=', Auth::user()->id)->pluck('id'))->get();
        return $lists;
        //return $tasks;
    }

    public function getAllLijsten(){

        $lists = Lists::get();

        return $lists;

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        return Lists::destroy($id);
    }

}
