<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Stephanie Schroé
 * @copyright  Copyright © 2014 Artevelde University College Ghent
 */

class FriendsController extends \BaseController {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    protected $layout = 'layouts.master';

    public function index()
    {
        $this->layout->content = View::make('friends', array(
            'users' => DB::table('users')->get(),
            'friends' => DB::table('friends')->get()
        ));


    }



    public function deleteFriend()
    {
        DB::table('friends')
            ->where('id', Input::get('delete'))
            ->update(array('defriended_at' => date("Y-m-d H:i:s")));

        return Redirect::route('friends');
    }

    public function addFriend()
    {
        DB::table('friends')
            ->insert(array('friends_id' => Input::get('add'), 'user_id' => Auth::user()->id) );

        return Redirect::route('friends');
    }

    public function updateFriend()
    {
        DB::table('friends')
            ->where('id', Input::get('update'))
            ->update(array('accepted_at' => date("Y-m-d H:i:s")));

        return Redirect::route('friends');
    }

    public function rejectFriend()
    {
        DB::table('friends')
            ->where('id', Input::get('reject'))
            ->update(array('rejected_at' => date("Y-m-d H:i:s")));

        return Redirect::route('friends');
    }
}
