<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Olivier Parent
 * @copyright  Copyright © 2014 Artevelde University College Ghent
 */

class ShareController extends \BaseController {

    /**
     * Display a listing of the resource.
     *

    $table->enum('choices', array('foo', 'bar'));



     * @return Response
     */
    protected $layout = 'layouts.master';


    /**
     * Weergeven pagina voor een nieuwe taak te maken
     *
     * @return Response
     */
    public function index(){
        $this->layout->content = View::make('listShare', array(
            'lists' => DB::table('lists')->get(),
            'friends' => DB::table('friends')->get()
        ));
    }



    public function share()
    {
       $array_lists = Input::get('list_selected_id');
       $friends_checked = Input::get('friends_id');

        foreach ($array_lists as $id) {
           DB::table('lists')->where('id', $id)->update(array('share'=> serialize($friends_checked)));
        }

        return Redirect::route('lists');
    }

    public function getSharedLists(){
        $list = Lists::where('share', '!=', "")->get();

        return $list;
    }
/*

// return Redirect::route('friendlist');
        //return Redirect::to('/friendshare');//->with('array_selected_tasks', $array_selected_tasks);

        $rules = array(
            'title' => 'required|min:3|max:255'
        );
        $validator = Validator::make(Input::all(), $rules);

        if($validator ->fails()){
            return Redirect::route('createTask')->withErrors($validator);
        }

        $task = new Task;
        $task->name        = Input::get('title');
        $task->description = Input::get('description');
        $task->tag_id      = Input::get('tag_id');
        $task->tag_text    = Input::get('tag_text');
        $task->due_at      = Input::get('deadline');
        $task->priority_id = Input::get('priority');
        $task->category_id = Input::get('category');
        $task->user_id     = Auth::user()->id;
        $task->save();

        return Redirect::route('home');*/


}