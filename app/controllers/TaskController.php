<?php
/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *                        aaaAAaaa            HHHHHH                         *
 *                     aaAAAAAAAAAAaa         HHHHHH                         *
 *                    aAAAAAAAAAAAAAAa        HHHHHH                         *
 *                   aAAAAAAAAAAAAAAAAa       HHHHHH                         *
 *                   aAAAAAa    aAAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   AAAAAa      AAAAAA                                      *
 *                   aAAAAAa     AAAAAA                                      *
 *                    aAAAAAAaaaaAAAAAA       HHHHHH                         *
 *                     aAAAAAAAAAAAAAAA       HHHHHH                         *
 *                      aAAAAAAAAAAAAAA       HHHHHH                         *
 *                         aaAAAAAAAAAA       HHHHHH                         *
 *                                                                           *
 *                                                                           *
 *                                                                           *
 *      a r t e v e l d e  u n i v e r s i t y  c o l l e g e  g h e n t     *
 *                                                                           *
 *                                                                           *
 *                                MEMBER OF GHENT UNIVERSITY ASSOCIATION     *
 *                                                                           *
 *                                                                           *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * @author     Stephanie Schroé
 * @copyright  Copyright © 2014 Artevelde University College Ghent
 */

class TaskController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

    protected $layout = 'layouts.master';

	public function index()
	{
//        return Task::all();
        $this->layout->content = View::make('tasks', array(
            'task' =>  DB::table('tasks')->get(),
            'lists' => DB::table('lists')->get()

        ));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return Redirect::to('/#page-task-create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $rules = [
            'name'            => 'required',
            'due_at'            => 'required'
        ];

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->passes()) {

            $task = new Task(Input::all());
            $task->user()->associate(Auth::user());
            $task->save();

            return Redirect::route('tasks');

            //
        } else {

            return Redirect::route('tasks');
        }


	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        return Task::find($id)
            ->load('user', 'pomodori', 'pomodori.labels')
        ;
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function edit($id)
    {


        $task = Task::findOrFail($id);
        return View::make('taskEdit', ['task' => $task, 'id' => $id]);

        //return Redirect::to('/#page-task-update', ['task' => $task, 'id' => $id]);
    }

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $test = "hallo";
        var_dump($test);


        $rules = [
            'name'            => 'required',
            'due_at'         => 'required'
        ];

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->passes()) {
            $task = Task::find($id);
            $task->name = Input::get('name');
            $task->due_at = Input::get('due_at');
            $task->lists_id = Input::get('lists_id');
            $task->tags = Input::get('tags');
            $task->prioriteit = Input::get('prioriteit');

            $task->save();

            return Redirect::route('tasks'); // Zie: $ php artisan routes
            //
        } else {

            return Redirect::route('task.update') // Zie: $ php artisan routes
                ->withInput()             // Vul het formulier opnieuw in met de Input.
                ->withErrors($validator); // Maakt $errors in View.
        }

	}

    public function finished($id)
    {
        $task = Task::find($id);
        $task->finished_at = New DateTime();
        $task->save();
        //return Redirect::to('index');
    }

    public function unfinished($id)
    {

        $task = Task::find($id);
        $task->finished_at = '0000-00-00 00:00:00';
        $task->save();
        //return Redirect::to('index');
    }

    public function getTasks(){
        $task = Task::orderBy('due_at', 'ASC')->where('user_id', '=', Auth::user()->id)->where('finished_at', '=', '0000-00-00 00:00:00')->get();

        return $task;
    }
    public function getAllTasks(){
        $task = Task::orderBy('due_at', 'ASC')->get();

        return $task;
    }

    public function getFinished(){
        $finished = Task::orderBy('due_at', 'ASC')->where('user_id', '=', Auth::user()->id)->where('finished_at', '!=', '0000-00-00 00:00:00')->get();
        return $finished;
    }
    /**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        return Task::destroy($id);
	}

}
